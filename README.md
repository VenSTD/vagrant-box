# README # 

## Description ##
It's dead simple way to make up **Vagrant** environment for LAMP server with chosen PHP version. The script saves much time of making up the environment.
The Box is based on **[venstd/trusty64](https://atlas.hashicorp.com/venstd/boxes/trusty64/)** box (or for Ubuntu 16.04 it's bento/ubuntu-16.04) which is the updated version of **ubuntu/trusty64** one.
## [Why Vagrant?](https://www.vagrantup.com/docs/why-vagrant/) ##
## [Getting Started with Vagrant](https://www.vagrantup.com/docs/getting-started/) ##
## Box Installation: ##
```
#!bash

git clone https://VenSTD@bitbucket.org/VenSTD/vagrant-box.git some-project
cd some-project
git fetch && git checkout 7.1.16.4a

```

After cloning in *Vagrantfile*:

1. Change php_version variable for appropriate one (5.5, 5.6, 7.0, 7.1.3 available)

2. Choose from the list packages which should be installed


At the end type: vagrant up

## Requirements ##
nfsd package must be installed (try *sudo apt-get install nfs-kernel-server* for ubuntu). For Windows there can appear some problems: [More here](https://www.vagrantup.com/docs/synced-folders/nfs.html)
# -*- mode: ruby -*-
# vi: set ft=ruby :

## Logowanie zewnetrzne do bazy mariadb (w PHPStormie):
# SSH Host: 127.0.0.1
# SSH Port: 2222
# SSH User: vagrant
# SSH Auth type Password: vagrant
# SSH Auth type Private Key: /.vagrant/default/virtualbox/private_key
# MySQL Host: 127.0.0.1
# MySQL Port: 3306 # Bo taki jest ustawiony jako forwarded_port w VagrantFile
# MySQL User: root
# MySQL Password: Haslo # Takie jakie ustawione na guest VM

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

# There are php and python environments to install
environment = "php" #or python

if (environment == "php")
	php_version = "7.1" # available php versions: 5.5, 5.6, 7.0, 7.1
elsif (environment == "python")
	# python is already installed into ubuntu
	# do some stuff
end


# if nfs mounting share problem occure, try to provide a static ip:
static_ip = "192.168.11.11"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

    # Set 1GB of memory for the box
    config.vm.provider "virtualbox" do |v|
        v.memory = 1024
        v.cpus = 2
    end


	# Every Vagrant virtual environment requires a box to build off of.
	config.vm.box = "bento/ubuntu-16.04"

	# Create a private network, which allows host-only access to the machine using a specific IP.
	if defined?(static_ip)
		config.vm.network "private_network", ip: static_ip
	else
		config.vm.network "private_network", type: "dhcp"
	end

	# Share an additional folder to the guest VM. The first argument is the path on the host to the actual folder.
	# The second argument is the path on the guest to mount the folder.
	config.vm.synced_folder "./", "/project"
	config.vm.synced_folder "./", "/var/www/html",
		type: "rsync",
		rsync__exclude: ".git/",
		id: "core",
		:nfs => true,
		:mount_options => ['nolock,vers=3,udp,noatime']

	# Define the bootstrap file: A (shell) script that runs after first setup of your box (= provisioning)
	config.vm.provision "shell", path: "provision/bootstrap-start.sh"

	# common things
	config.vm.provision "shell", path: "provision/git.sh"
	config.vm.provision "shell", path: "provision/vim.sh"
	config.vm.provision "shell", path: "provision/nano.sh"
	config.vm.provision "shell", path: "provision/mc.sh"

	if (environment == "php")
		# available php version: 5.5, 5.6, 7.0
		# list of installed packages:
		config.vm.provision "shell", path: "provision/apache2.sh"
		config.vm.provision "shell", path: "provision/mysql.sh"
		config.vm.provision "shell", path: "provision/php" + php_version + ".sh"
		config.vm.provision "shell", path: "provision/php" + php_version + "-extensions.sh"
		config.vm.provision "shell", path: "provision/phpmyadmin.sh" # pma must be after php!
		config.vm.provision "shell", path: "provision/composer.sh"
		config.vm.provision "shell", path: "provision/tesseract.sh"
	elsif (environment == "python")
		config.vm.provision "shell", path: "provision/mysqldb-python.sh"
		config.vm.provision "shell", path: "provision/scrapy.sh"
		config.vm.provision "shell", path: "provision/pip.sh"
	end

	config.vm.provision "shell", path: "provision/bootstrap-end.sh"
end
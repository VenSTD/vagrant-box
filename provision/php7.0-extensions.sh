#!/usr/bin/env bash
# Instalacja modulow do php i przeniesienie php.ini z cli do apache2

echo "_____________ INSTALACJA PHP EXTENSIONS _____________"

sudo apt-get install -y \
libapache2-mod-php7.0 \
php7.0-cli \
php7.0-intl \
php7.0-mcrypt \
php7.0-curl \
php7.0-cgi \
php7.0-common \
php7.0-fpm \
php7.0-gd \
php7.0-bz2 \
php7.0-json \
php7.0-mysql \
php7.0-mbstring \
php7.0-zip \
php7.0-xml \
php7.0-tidy \
php7.0-opcache \
php-imagick \
php-xdebug \
php-redis \
php-memcached \
php-pear

# Konfiguracja xdebug
PHPINFO=$(cat <<EOF
xdebug.remote_enable = on
xdebug.remote_connect_back = on
xdebug.idekey = "PHPSTORM"
EOF
)
echo "$PHPINFO" >> /etc/php/7.0/mods-available/xdebug.ini

# Skopiowanie ustawień konsolowych do apache'owych
sudo cp /etc/php/7.0/cli/php.ini /etc/php/7.0/apache2/php.ini

# sudo a2enmod proxy_fcgi setenvif
sudo a2enconf php7.0-fpm
#!/usr/bin/env bash

export BOX_PROJECT_FOLDER='/var/www/html'

echo "_____________ BOOTSTRAP END _____________"

# create phpinfo() file
PHPINFO=$(cat <<EOF
<?php phpinfo(); ?>
EOF
)
echo "$PHPINFO" > ${BOX_PROJECT_FOLDER}/info.php 

# redirect automatically after log in to the project directory:
REDIRECT=$(cat <<EOF
# cd to the /var/www/html directory after ssh
echo -e "\E[1;33mYou are in the project directory now"
tput sgr0
cd /project
EOF
)
sudo echo "$REDIRECT" >> ~/.bashrc

# Bindowanie adresu dla mysql
sudo sed -i 's/bind-address.*/bind-address = 0.0.0.0/' /etc/mysql/my.cnf
sudo service mysql restart

# enable mod_rewrite
sudo a2enmod rewrite

# restart apache
service apache2 restart
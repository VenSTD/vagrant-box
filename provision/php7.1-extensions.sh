#!/usr/bin/env bash
# Instalacja modulow do php i przeniesienie php.ini z cli do apache2

echo "_____________ INSTALACJA PHP EXTENSIONS _____________"

sudo apt-get install -y \
libapache2-mod-php7.0 \
php7.1-cli \
php7.1-intl \
php7.1-mcrypt \
php7.1-curl \
php7.1-cgi \
php7.1-common \
php7.1-fpm \
php7.1-gd \
php7.1-bz2 \
php7.1-json \
php7.1-mysql \
php7.1-mbstring \
php7.1-zip \
php7.1-xml \
php7.1-tidy \
php7.1-opcache \
php-imagick \
php-xdebug \
php-redis \
php-memcached \
php-pear

# Konfiguracja xdebug
PHPINFO=$(cat <<EOF
xdebug.remote_enable = on
xdebug.remote_connect_back = on
xdebug.idekey = "PHPSTORM"
EOF
)
echo "$PHPINFO" >> /etc/php/7.1/mods-available/xdebug.ini

# Skopiowanie ustawień konsolowych do apache'owych
sudo cp /etc/php/7.1/cli/php.ini /etc/php/7.1/apache2/php.ini

# sudo a2enmod proxy_fcgi setenvif
sudo a2enconf php7.1-fpm
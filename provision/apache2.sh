#!/usr/bin/env bash

echo "_____________ INSTALACJA APACHE 2 _____________"

export BOX_PROJECT_FOLDER='/var/www/html'

# install apache 2.5
sudo apt-get install -y apache2

# setup hosts file
VHOST=$(cat <<EOF
<VirtualHost *:80>
    DocumentRoot "${BOX_PROJECT_FOLDER}"
    <Directory "${BOX_PROJECT_FOLDER}">
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
EOF
)
echo "${VHOST}" > /etc/apache2/sites-available/000-default.conf

sudo a2enmod proxy_fcgi setenvif
#!/usr/bin/env bash

echo "_____________ INSTALACJA PHP 5.6 _____________"

export BOX_PHP_VERSION='5.6'

sudo add-apt-repository ppa:ondrej/php
sudo apt-get update

# install php 5.6 from ondrej repository
sudo apt-get install -y php${BOX_PHP_VERSION}
sudo apt-get install php${BOX_PHP_VERSION}-mysql

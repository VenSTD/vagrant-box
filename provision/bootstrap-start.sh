#!/usr/bin/env bash

# Use single quotes instead of double quotes to make it work with special-character passwords
# export BOX_PASSWORD='admin123'
# export BOX_PROJECT_FOLDER='/var/www/html'

# create project folder
# sudo mkdir "${BOX_PROJECT_FOLDER}"

echo "_____________ BOOTSTRAP START _____________"
# update / upgrade
sudo apt-get install software-properties-common
sudo apt-get install python-software-properties
sudo apt-get update
sudo apt-get -y upgrade
#!/usr/bin/env bash

export BOX_PASSWORD='admin123'

# install mysql and give password to installer

echo "_____________ INSTALACJA MYSQL _____________"

sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $BOX_PASSWORD"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $BOX_PASSWORD"
sudo apt-get -y install mysql-server
#!/usr/bin/env bash
# Instalacja modulow do php i przeniesienie php.ini z cli do apache2
sudo apt-get install -y \
php5.6-cli \
php5.6-intl \
php5.6-mcrypt \
php5.6-curl \
php5.6-cgi \
php5.6-common \
php5.6-fpm \
php5.6-gd \
php5.6-mysql \
php5.6-mbstring \
php-imagick \
php-xdebug \
php-zip \
php-xml \
php-dom

# Konfiguracja xdebug
PHPINFO=$(cat <<EOF
xdebug.remote_enable = on
xdebug.remote_connect_back = on
xdebug.idekey = "PHPSTORM"
EOF
)
echo "$PHPINFO" >> /etc/php/5.6/mods-available/xdebug.ini

# php5-xdebug php5-imagick php-pear
sudo cp /etc/php5/cli/php.ini /etc/php5/apache2/php.ini
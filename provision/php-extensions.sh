#!/usr/bin/env bash
# Instalacja modulow do php i przeniesienie php.ini z cli do apache2
sudo apt-get install -y \
php5-cli \
php5-intl \
php5-mcrypt \
php-pear \
php5-curl \
php5-imagick \
php5-cgi \
php5-common \
php5-fpm \
php5-gd \
php5-mysql \
php5-xdebug

sudo cp /etc/php5/cli/php.ini /etc/php5/apache2/php.ini
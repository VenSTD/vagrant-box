#!/usr/bin/env bash
# Instalacja modulow do php i przeniesienie php.ini z cli do apache2
sudo apt-get install -y \
    php5.5-cli \
    php5.5-intl \
    php5.5-mcrypt \
    php5.5-curl \
    php5.5-cgi \
    php5.5-common \
    php5.5-fpm \
    php5.5-gd \
    php5.5-mysql \
    php5.5-mbstring \
    php5-xdebug \
    php5-imagick \
    php-pear

sudo cp /etc/php5/cli/php.ini /etc/php5/apache2/php.ini
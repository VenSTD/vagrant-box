#!/usr/bin/env bash
echo "_____________ INSTALACJA PHP 7.0 _____________"

export BOX_PHP_VERSION='7.0'

sudo add-apt-repository ppa:ondrej/php
sudo apt-get update

# install php 7.0 from ondrej repository
sudo apt-get install -y php${BOX_PHP_VERSION}
sudo apt-get install php${BOX_PHP_VERSION}-mysql

#!/usr/bin/env bash
# install Composer

echo "_____________ INSTALACJA COMPOSER _____________"

curl -s https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

composer global require "fxp/composer-asset-plugin:~1.1.1"